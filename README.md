# Maxwell VSH

The **Maxwell VSH** (Maxwell Vector Spherical Harmonics) package is a software library written in Matlab to work with 
expansions of solutions to the time-harmonic Maxwell equations in terms of vector spherical harmonics.

The package provides a set of classes to generate entire or radiating solutions to the Maxwell system or to compute
best approximations to a given solution on a given sphere. Also, far fields of radiating fields can be easily computed.

## License

Copyright (c) 2023, Tilo Arens

This software is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.
This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
License for more details.
You should have received a copy of the GNU General Public License along with this software package, contained
in the file LICENSE. If not, see https://www.gnu.org/licenses/.

## Getting started

To use **Maxwell VSH,** add the directory containing this README.md file to your Matlab path.

## What is where?

The library is organized as a number of Matlab packages which are listed below. Each package contains specific types of 
classes. A Matlab package *package_name* is always contained in a directory ``+package_name/``.

- **examples** These are complete example programs illustrating a particular use case.
- **fields** The classes in this package represent particular types of vector fields. They are either defined in the
  entire space (abstract class VectorField) or on the surface of the unit sphere (abstract class VectorFieldOnSphere).
  There are concrete fields (PlaneWave, HertzDipole) as well as fields represented as expansions of vector spherical 
  harmonics (EntireWaveField, RadiatingWaveField, L2TangField).
- **quadrature** Functions that provide quadrature points and weights.
- **scattering** Functions to solve scattering problems for a spherical scatterer or to compute a far field operator.
- **specialfunc** Functions to evaluate spherical harmonics.
- **test** Unit tests.
- **util** Miscellaneous utilities.
