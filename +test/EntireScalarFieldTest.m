%> @file EntireWaveFieldTest.m
%> @brief Tests for the EntireWaveField class
% ======================================================================
%> @brief Tests the functionality of the EntireWaveField class.
% ======================================================================
classdef EntireScalarFieldTest < matlab.unittest.TestCase
   
    methods (Test)
        
        
        %> @brief Compute and check accuracy of the evaluation of a plane wave.
        %>
        %> A plane wave is defined and its expansion coefficients as an
        %> EntireScalarFunction are computed. The result is checked for
        %> accuracy in 100 random points in a ball.
        function testEvaluation(testcase)
        
            % compute random wave number, direction of incidence and polarization
            rng('shuffle');
            
            z = rand(3,1);
            % R = 3;
            
            theta_d = pi * rand;
            phi_d = -pi + 2*pi*rand;
            d = [ cos(phi_d) * sin(theta_d); sin(phi_d) * sin(theta_d); cos(theta_d) ];
            
            k = 5 * rand;

            N = 25;
            
            a_nm = zeros(1,(N+1)^2);

            [d_phi, d_th, ~] = cart2sph(d(1), d(2), d(3));
            d_th = pi/2 - d_th;

            for n=0:N
                m_range = n^2+1:(n+1)^2;

                Y = specialfunc.sphericalHarmonics(d_th, d_phi, n);
                a_nm(m_range) = 4*pi * (1i)^n * Y' * exp(1i * k * dot(z,d));
            end
            
            esf = fields.EntireScalarField(k, a_nm, z);
            
            % Evaluate both fields in 100 random points in ball of radius 1.5 and compare results            
            theta = pi * rand(100,1);
            phi = -pi + 2*pi*rand(100,1);
            r = 1.5 * rand(100,1);            
            
            X1 = z(1) + r .* cos(phi) .* sin(theta); 
            X2 = z(2) + r .* sin(phi) .* sin(theta); 
            X3 = z(3) + r .* cos(theta);
            
            PW = exp(1i * k * (d(1)*X1 + d(2)*X2 + d(3)*X3));
            ESF = esf.eval(X1, X2, X3);
            
            err = abs(PW - ESF);
            fprintf('Maximal error: %e\n', max(err) );
            
            testcase.verifyLessThan(err,1e-5);
            
        end
        
        %> @brief Compute and check accuracy of the approximation of a plane wave.
        %>
        %> A plane wave is defined and its best approximation by an
        %> EntireWaveFunction is computed. The result is checked for
        %> accuracy in 100 random points in a ball.
        function testBestApproximation(testcase)
            
            % Arguments for expansion
            z = [1; -0.2; 0.3];
            R = 2;
            N = 21;
        
            % compute random wave number, direction of incidence and polarization
            rng('shuffle');
            
            theta_d = pi * rand;
            phi_d = -pi + 2*pi*rand;
            d = [ cos(phi_d) * sin(theta_d); sin(phi_d) * sin(theta_d); cos(theta_d) ];
            
            k = 5 * rand;

%             d = [ 1; 0; 0];
%             p = [ 0; 1; 0];
%             k = 4*sqrt(2);

            [ phi_d, theta_d, ~ ] = cart2sph( d(1), d(2), d(3) );
            theta_d = pi/2 - theta_d;
            
            % compute the exact coefficients of this plane wave.      
            a_nm = zeros(1,(N+1)^2);
            
            for n=0:N

                m_range = n^2+1:(n+1)^2;

                Y = specialfunc.sphericalHarmonics(theta_d, phi_d, n);
                a_nm(m_range) = 4 * pi * (1i)^n * Y' * exp( 1i * k * dot(z,d) );

            end
            
            % define plane wave
            pw = fields.PlaneWaveScalar(k, d);
            
            % compute EntireWaveField
            esf = fields.EntireScalarField.bestApprox(pw, k, N, z, R);
            
            % Evaluate both fields in 100 random points in ball of radius 1.5 and compare results            
            theta = pi * rand(100,1);
            phi = -pi + 2*pi*rand(100,1);
            r = 1.5 * rand(100,1);            
            
            X1 = z(1) + r .* cos(phi) .* sin(theta); 
            X2 = z(2) + r .* sin(phi) .* sin(theta); 
            X3 = z(3) + r .* cos(theta);
            
            PW = pw.eval(X1, X2, X3);
            ESF = esf.eval(X1, X2, X3);
            
            err = abs(PW - ESF);
            fprintf('Maximal error: %e\n', max(err) );
            
            testcase.verifyLessThan(err,1e-5);
            
        end
        
    end
end