%> @file RadiatingScalarFieldTest.m
%> @brief Tests for the RadiatingScalarField class
% ======================================================================
%> @brief Tests the functionality of the RadiatingScalarField class.
% ======================================================================
classdef RadiatingScalarFieldTest < matlab.unittest.TestCase
   
    methods (Test)        
        
        %> @brief Compute and check accuracy of the approximation of a
        %> point source.
        %>
        %> A point source is defined and its best 
        %> approximation by a RadiatingScalarFunction is computed. The result 
        %> is checked for accuracy in 100 random points outside the sphere
        %> on which the best approximation is computed, and again on a
        %> sphere with the same center but a larger radius.
        function testBestApproximation(testcase)
            
            % Arguments for expansion
            z = [0.0; 0.0; 0.5];
            R = 3;
            N = 19;
        
            % set random wave number and source point for the point source
            rng('shuffle');
            
            y = -1 + 2*rand(3,1);
            k = 5 * rand;
            
            % define the point source
            ps = fields.HelmholtzPointSource(k, y);
            
            % compute RadiatingScalarField
            rsf = fields.RadiatingScalarField.bestApprox(ps, k, N, z, R); 
            rsf.printNormInDegree();
            
            % Evaluate both fields in 100 random points in a shell of minimal 
            % radius 3 and maximal radius 6 and compare results            
            theta = pi * rand(100,1);
            phi = -pi + 2*pi*rand(100,1);
            r = R +  3 * rand(100,1);            
            
            X1 = z(1) + r .* cos(phi) .* sin(theta); 
            X2 = z(2) + r .* sin(phi) .* sin(theta); 
            X3 = z(3) + r .* cos(theta);
 
            PS = ps.eval(X1, X2, X3);
            RSF = rsf.eval(X1, X2, X3);
            
            err = abs(PS - RSF);
            fprintf('Maximal error: %e\n', max(err) );
            
            testcase.verifyLessThan(err,1e-5);
            
        end
        
        %> @brief Compute and check accuracy for evaluations of the far fields
        %> of first degree radiating wave fields.
        %>
        %> The three first order radiating scalar fields (Order 1 spherical Hankel 
        %> function times first degree spherical harmonic) for random
        %> centers of expansion are approximated on a sphere around the
        %> origin, the far field coefficients of the approximation are
        %> computed and the far field is evaluated in 100 random points on
        %> the unit sphere. The results are compared to the exact values.
        function testFarFieldEvaluation(testcase)
                                
            N = 15;
            
            % compute random wave number and center of expansion
            rng('shuffle');
            
            y = -1 + 2*rand(3,1);
            k = 5 * rand;
            
%             y = [0;0;0];
%             k = 3*sqrt(2);

            % Define the wave fields
            rsf = cell(1, 3);
            for j=1:3
                alpha = zeros(1, 4);
                alpha(j+1) = 1;
                rsf{j} = fields.RadiatingScalarField(k, alpha, y);
            end
            
            % Compute best approximations and their far fields
            R = 4;
            z = [0; 0; 0];
            bestApprox = cell(1, 3);
            
            for j=1:3
                bestApprox{j} = fields.RadiatingScalarField.bestApprox(rsf{j}, k, N, z, R);

            end
            
            farFieldError = zeros(100, 3);
            
            % Evaluate far fields in 100 random points on unit sphere and compare results            
            theta = pi * rand(100,1);
            phi = -pi + 2*pi*rand(100,1);
            
            % Phase factor to compensate for center of expansion not in the
            % origin.
            factorCenterExp = exp( -1i * k * ( cos(phi) .* sin(theta) * y(1) + sin(phi) .* sin(theta) * y(2) + cos(theta) * y(3) ) );
            
            for j=1:3
                Y = specialfunc.sphericalHarmonics(theta.', phi.', 1);
                V_infty = rsf{j}.evalFarFieldPattern(theta, phi);
                farFieldError(:, j) = abs(V_infty - 4*pi/k * factorCenterExp .* Y(j, :).' );
            end
            
            fprintf('Maximum Far Field Error: %g\n', max(farFieldError));            
            testcase.verifyLessThan(max(max(farFieldError)),1e-5);
            
        end
    end
    
end
