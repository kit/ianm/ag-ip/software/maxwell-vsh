%> @file scatPenetrablePotentialApproach.m
%> @brief Contains the scatPenetrablePotentialApproach() function.
% ======================================================================
%> @brief The electromagnetic scattering problem for a penetrable sphere is
%> solved using the representations via scalar/vector potentials of Garcia
%> d'Abajo and Howie.
%>
%> @param[in] phi_inc Incident scalar potential with the center 
%>    of the sphere as center of expansion.
%> @param[in] a_inc Incident vector potential with the center 
%>    of the sphere as center of expansion.
%> @param[in] R Radius of the penetrable sphere.
%> @param[in] eps_r relative permittivity inside the sphere.
%> @param[out] phi_scat The scattered scalar potential
%> @param[out] a_scat The scattered vector potential
%> @param[out] phi_trans The transmitted scalar potential
%> @param[out] a_trans The transmitted vector potential
function [phi_scat, a_scat, phi_trans, a_trans] = scatPenetrablePotentialApproach(phi_inc, a_inc, R, eps_r)

phi_scat = [];
a_scat = [];
phi_trans = [];
a_trans = [];

end