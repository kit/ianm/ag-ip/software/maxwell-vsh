%> @file scatAcousticFromPenetrableSphere.m
%> @brief Contains the scatAcousticFromPenetrableSphere() function.
% ======================================================================
%> @brief For a given EntireScalarField as the incident acoustic field,
%> compute the scattered field for scattering from a penetrable homogenous 
%> sphere.
%>
%> @param[in] u_inc Incident acoustic field, an EntireScalarField with the center 
%>    of the sphere as center of expansion.
%> @param[in] R Radius of the penetrable sphere.
%> @param[in] ref_index refractive index of the material inside the sphere.
%> @param[out] u_scat The scattered acoustic field, a RadiatingScalarField
%> @param[out] u_trans The transmitted acoustic field, an EntireScalarField
function [u_scat, u_trans] = scatAcousticFromPenetrableSphere(u_inc, R, ref_index)

alpha = zeros(1, (u_inc.degree+1)^2);
beta  = zeros(1, (u_inc.degree+1)^2);

wavenum = u_inc.wavenum;
wavenum_inside = ref_index * u_inc.wavenum;

% initial step: n = m = 0
j_0_out = sqrt(pi / (2*wavenum*R)) * besselj(1/2, wavenum*R);
j_1_out = sqrt(pi / (2*wavenum*R)) * besselj(3/2, wavenum*R);
h_0_out = sqrt(pi / (2*wavenum*R)) * besselh(1/2, 1, wavenum*R);
h_1_out = sqrt(pi / (2*wavenum*R)) * besselh(3/2, 1, wavenum*R);
j_0_in = sqrt(pi / (2*wavenum_inside*R)) * besselj(1/2, wavenum_inside*R);
j_1_in = sqrt(pi / (2*wavenum_inside*R)) * besselj(3/2, wavenum_inside*R);

matrix = [ h_0_out, -j_0_in; -h_1_out*wavenum, -j_1_in*wavenum_inside ];
rhs = -u_inc.alpha(1) * [j_0_out; -j_1_out*wavenum];

res = matrix \ rhs;
alpha(1) = res(1);
beta(1) = res(2);

for n=1:u_inc.degree
    
    m_range = n^2+1:(n+1)^2;

    j_n_out = sqrt(pi / (2*wavenum*R)) * besselj(n+1/2, wavenum*R);
    j_n_min_1_out = sqrt(pi / (2*wavenum*R)) * besselj(n-1/2, wavenum*R);
    h_n_out = sqrt(pi / (2*wavenum*R)) * besselh(n+1/2, 1, wavenum*R);
    h_n_min_1_out = sqrt(pi / (2*wavenum*R)) * besselh(n-1/2, 1, wavenum*R);
    j_n_in = sqrt(pi / (2*wavenum_inside*R)) * besselj(n+1/2, wavenum_inside*R);
    j_n_min_1_in = sqrt(pi / (2*wavenum_inside*R)) * besselj(n-1/2, wavenum_inside*R);
    
    matrix = [h_n_out, -j_n_in; wavenum*h_n_min_1_out - (n+1)/R*h_n_out, -wavenum_inside*j_n_min_1_in + (n+1)/R*j_n_in];
    rhs = [j_n_out; wavenum*j_n_min_1_out - (n+1)/R*j_n_out] * (-u_inc.alpha(m_range));

    res = matrix \ rhs;
    alpha(m_range) = res(1, :);
    beta(m_range) = res(2, :);
    
end

u_scat = fields.RadiatingScalarField(wavenum, alpha, u_inc.z);
u_trans = fields.EntireScalarField(wavenum_inside, beta, u_inc.z);

end
