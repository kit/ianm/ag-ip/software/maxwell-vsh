%> @file HelmholtzPointSource.m
%> @brief Contains the HelmholtzPointSource class.
% ======================================================================
%> @brief Implementation of a ScalarField as a point source.
%>
%> The HelmholtzPointSource represents the acoustic field due to a point
%> source at position y.
% ======================================================================
classdef HelmholtzPointSource < fields.ScalarField
    
    
    properties (SetAccess = protected)
        
        %> @brief The wave number
        wavenum = 1;
        
        %> @brief Source point
        y = [0;0;0];
               
    end
    
    methods
        
        function ps = HelmholtzPointSource(wavenum, y)
            
            if (nargin > 0)
                ps.wavenum = wavenum;
            end
            
            if ( nargin > 1 )
                ps.y = y;
            end
            
        end
        
        function V = eval(ps, X1, X2, X3)
            
            Z1 = X1 - ps.y(1);
            Z2 = X2 - ps.y(2);
            Z3 = X3 - ps.y(3);

            R = sqrt( Z1.^2 + Z2.^2 + Z3.^2 );

            V = exp(1i * ps.wavenum * R) ./ (4*pi * R);
            
        end
        
        
        function ps_inf = farFieldPattern(ps, Theta, Phi)
            
            theta = Theta(:);
            phi = Phi(:);
            
            Z1 = cos(phi) .* sin(theta);
            Z2 = sin(phi) .* sin(theta);
            Z3 = cos(theta);
            
            phaseFactor = exp(-1i * ps.wavenum * ( ps.y(1)*Z1 + ps.y(2)*Z2 + ps.y(3)*Z3 ));            
            ps_inf = 4*pi * phaseFactor;
            
            ps_inf = reshape( ps_inf, size(Theta) );
            
        end
        
    end
    
end