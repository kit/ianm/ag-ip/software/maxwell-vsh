%> @file RadiatingScalarField.m
%> @brief Contains the RadiatingScalarField class.
% ======================================================================
%> @brief Class representing a radiating scalar field defined outside a
%> given ball.
%>
%> Radiating wave functions are linear combinations of the functions
%>
%> @f[
%>   v_n^m = h_n^{(1)}(k |x|) \, Y_n^m(\hat{x}) 
%> @f]
%>
%> which are solutions to the Helmholtz equation. More precisely, we shift
%> these functions to have an arbitrary center of expansion z. Hence, an 
%> object of this class represents a function
%>
%> @f[
%>  V(x) = \sum_{n=0}^N \sum_{m = -n}^n \alpha_n^m \, v_n^m(x)
%> @f]
%>
%> Typically, the coefficients are computed by a best approximation on a
%> sphere of radius R around z. Then
%>
%> @f[
%>  \alpha_n^m = - \int_{S^2} \frac{ v(z + R \hat{x}) \, Y_n^{-m}(\hat{x}) }{ h_n^{(1)}(kR) } \,
%>   ds(\hat{x}) \, .
%> @f]
%
% ======================================================================
classdef RadiatingScalarField < fields.ScalarField
    
    properties (SetAccess = protected)
        
        %> @brief The wave number
        wavenum = 1;
        
        %> @brief The degree of the highest scalar spherical harmonic in
        %> the expansion.
        degree = 1;
        
        %> @brief The coordinates of the center of expansion, a 3
        %> coordinate column vector.
        z = [0; 0; 0];
        
        %> @brief The coefficients for the functions @f$ v_n^{m} @f$.
        %>
        %> These form a row vector of length (degree+1)^2 and are ordered
        %> like
        %>
        %> a_0^0 a_1^{-1} a_1^{0} a_1^{1} a_2^{-2} ... a_2^{2} a_3^{-3} ... a_degree^{degree}
        %>
        alpha = [];
        
    end
    
    
    methods
        
        %> @brief Construction
        %>
        %> Precise behaviour depends on the number of arguments:
        %> - at least 1 argument:  field around the origin
        %> - at least 3 arguments: field around z
        %>
        %> If at least 2 arguments are given, the coefficients are set.
        %>
        %> Otheriwse, all coeffients are initialized to 0.
        function rsf = RadiatingScalarField(wavenum, alpha, z)            
            
            if ( nargin >= 1 )
                rsf.wavenum = wavenum;
            end
            if ( nargin >= 2 )
                rsf.setCoeffs(alpha);                
            end
            
            if ( nargin >= 3 )
                rsf.setCenter(z);
            end
            
        end
        
        %> @brief Set center of expansion and minimal radius
        function setCenter(rsf, z)
            
            assert(length(z) == 3, 'Center points must be vector of dimension 3.')
            rsf.z = [z(1); z(2); z(3)];

        end
        
        %> @brief Set degree and expanson coefficients
        function setCoeffs(rsf, alpha)
            
            assert(mod(sqrt(length(alpha)), 1) == 0, 'Length of coefficient vector must be a square number.');
                        
            rsf.alpha = alpha;
            rsf.degree = sqrt(length(alpha)) - 1;            
            
        end
        
        %> @brief Implementation of scalarField.eval()
        function V = eval( rsf, X1, X2, X3 )
            
            x1 = X1(:).' - rsf.z(1);
            x2 = X2(:).' - rsf.z(2);
            x3 = X3(:).' - rsf.z(3);

            V = zeros(1,length(x1));

            [phi, theta, r] = cart2sph(x1, x2, x3);
            theta = pi/2 - theta;

            for n=0:rsf.degree
                mRange = n^2+1:(n+1)^2;

                % Compute Hankel functions
                h_n  = sqrt(pi / (2*rsf.wavenum) ./ r ) .* besselh(n+1/2, 1, rsf.wavenum*r);

                Y_x = specialfunc.sphericalHarmonics(theta, phi, n);

                V = V + h_n .* (rsf.alpha(mRange) * Y_x);
            end
            
            % Reshape output to size of the input
            V = reshape(V, size(X1));

        end
        
        
        %> @brief Coefficients of the far field pattern of the RadiatingScalarField
        %>
        %> F^\infty = \sum a_n^m U_n^m + b_n^m V_n^m
        %>
        %> This function computes the coefficients for the far field
        %> asympotics using z as the origin.
        function a = farFieldCoeffs(rsf)
            
            preFac_alpha = ones(1,(rsf.degree+1)^2);
            
            for n=1:rsf.degree
                mRange = n^2+1:(n+1)^2;              
                preFac_alpha(mRange) = -4*pi/rsf.wavenum * (-1i)^(n+1);
            end
            
            a = preFac_alpha .* rsf.alpha;
            
        end
        
        %> @brief Evaluation of the far field pattern of this wave field
        %>
        %> For the far field asympotics, the point y is used as the origin.
        %> If this argument is omitted, y = [0;0;0] is assumed.
        %>
        %> The far field pattern is evaluated for the directions given by 
        %> @a theta and @a phi in angular coordinates. This is done by
        %> obtaining the far field as an L2TangField if z were at the origin. 
        %> This pattern is evaluated and then multiplied by the appropriate 
        %> phase shift.
        function V_infty = evalFarFieldPattern(rsf, Theta, Phi, y)
            
            theta = Theta(:).';
            phi = Phi(:).';

            if (nargin < 4)
                y = [0; 0; 0];
            end
            
            a = rsf.farFieldCoeffs;
            V_infty = zeros(size(theta));

            for n=0:rsf.degree
                mRange = n^2+1:(n+1)^2;
                Y = specialfunc.sphericalHarmonics(theta, phi, n);
                V_infty = V_infty + a(mRange) * Y;
            end
            
            phaseFactor = exp( -1i * rsf.wavenum ...
                * ( cos(phi) .* sin(theta) * ( rsf.z(1) - y(1) ) ...
                  + sin(phi) .* sin(theta) * ( rsf.z(2) - y(2) ) ...
                  + cos(theta) * ( rsf.z(3) - y(3) ) ) );            
              
            V_infty = phaseFactor .* V_infty;
            
            % Reshape output to size of the input
            V_infty = reshape(V_infty, size(Theta));
            
        end % of function V_infty = evalFarFieldPattern(rsf, Theta, Phi, y)
        
        
        %> @brief Print norm in each degree
        function printNormInDegree(rsf)
            
            for n = 0:rsf.degree                
                mRange = n^2+1:(n+1)^2;
                fprintf('  %10.4e', sqrt(sum(abs(rsf.alpha(mRange)).^2) ));
            end
            
            fprintf('\n');            
        end
                       
    end % of methods
    
    
    methods (Static)
        
        %> @brief Compute the best approximation of a given scalarField by a RadiatingScalarField
        %>
        %> The ScalarField @a sf is approximated by a RadiatingScalarField
        %> outside the ball of radius R centered at z. This done by computing 
        %> the best approximation to sf in the space spanned by the basis functions of 
        %> degree less than or equal to @a N on the sphere of radius R centered 
        %> at z  in the L^2-norm (i.e. compute an orthogonal projection)
        %>
        %> The function performs integration on the unit sphere with a
        %> tensor product quadrature rule, with a Gauss-Legendre rule in the 
        %> polar angle and a composite trapezoidal rule in the azimuthal angle. 
        %> 2*Nazim points in the
        %> azimuthal and Npolar points in the polar direction are used. If these
        %> arguments are not specified, a default value of 100 points in polar
        %> and 200 points in azimuthal direction is used.
        function rsf = bestApprox(sf, wavenum, N, z, R, Npolar, Nazim)
                    
            % obtain the quadrature points and weights
            if ( nargin >= 6 )
                [Theta, Phi, W] = quadrature.sphereGaussLegTrap(Npolar, Nazim);
            else
                [Theta, Phi, W] = quadrature.sphereGaussLegTrap(100, 100);
            end         
            
            X1 = z(1) + R * cos(Phi) .* sin(Theta);
            X2 = z(2) + R * sin(Phi) .* sin(Theta);
            X3 = z(3) + R * cos(Theta);             
            
            % evaluate the scalarField in the quadrature points
            F = sf.eval(X1, X2, X3);   
            
            % intialize coefficients
            alpha = zeros(1,(N+1)^2);
            
            % for each vector spherical harmonic up to degree N
            for n=0:N
                
                % scaling of coefficients on sphere of radius R
                h_n = sqrt(pi / (2*wavenum) / R ) * besselh(n+1/2,1,wavenum*R);
                
                Wmat = W * ones(1,2*n+1);
                
                % evaluate the sh on the quadrature points
                Y = specialfunc.sphericalHarmonics(Theta, Phi, n);
                
                % scalar producs in L^2_tangential
                a = F * (Y' .* Wmat);

                % set the coefficients
                m_range = n^2+1:(n+1)^2;
                alpha(m_range) = a / h_n;
                
            end
            
            % define the RadiatingScalarField
            rsf = fields.RadiatingScalarField(wavenum);
            rsf.setCenter(z);
            rsf.setCoeffs(alpha);
            
        end
        
        
    end % of methods (Static)
    
end
