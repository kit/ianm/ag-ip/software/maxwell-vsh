%> @file ScalarField.m
%> @brief Contains the ScalarField class.
% ======================================================================
%> @brief Abstract class representing a scalar field that can be evaluated.
% ======================================================================
classdef ScalarField < handle
    % classdef ScalarField < handle
    
    methods (Abstract)
        
        %> @brief Evaluation of the scalar field
        %>
        %> Returns matrix U of the same size as X1, X2, X3
        %> containing the values of the scalar field in
        %> the points given by the Xj
        %>
        %> @param[in] sf The ScalarField instance.
        %> @param[in] X1 Matrix of x1 coordinates of the points at which
        %>   evaluation takes place.
        %> @param[in] X2 Matrix of x2 coordinates, same size as X1.
        %> @param[in] X3 Matrix of x3 coordinates, same size as X1.
        %> @param[out] U Result, same size as X1.
        U = eval(sf, X1, X2, X3);
        
    end
    
end

