%> @file PlaneWaveScalar.m
%> @brief Implementation of a ScalarField as a plane wave.
% ======================================================================
%> @brief Simple implementation of ScalarField as a plane wave.
%>
%> A scalar plane wave is defined by wave number and direction of
%> propagation. The latter is a column vector of length three.
% ======================================================================
classdef PlaneWaveScalar < fields.ScalarField
    
    
    properties (SetAccess = protected)
        
        %> @brief The wave number
        wavenum = 1;
        
        %> @brief Direction of propagation
        d = [1; 0; 0];
               
    end
    
    methods
        
        %> @brief Construction with optional initialization of parameters.
        %
        % The constructor takes arguments @a wavenum (the wave number), 
        % @a d (direction of incidence). The latter argument should be a
        % column vector of length 3 and must have Euclidean norm 1. Default 
        % values, if these parameters are omited, are  @a wavenum = 1 and 
        % @a d = [1; 0; 0].
        function pw = PlaneWaveScalar(wavenum, d)
            
            if ( nargin > 0 )
                pw.wavenum = wavenum;
            end
            
            if ( nargin > 1 )
                pw.d = d;
            end
                       
            if ( abs( norm(pw.d) - 1.0 ) > 1e-14 )
                error('PlaneWaveScalar construction: direction of propagation must have norm 1.');
            end
            
        end
        
        %> @brief Implementation of ScalarField.eval()
        function V = eval(pw, X1, X2, X3)
            
            V = exp( 1i * pw.wavenum * ( pw.d(1) * X1 + pw.d(2) * X2 + pw.d(3) * X3 ) );
            
        end
        
    end
    
end