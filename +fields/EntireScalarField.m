%> @file EntireScalarField.m
%> @brief Contains the EntireScalarField class.
% ======================================================================
%> @brief Class representing a scalar field that is an entire function
%> in a given ball.
%>
%> Entire scalar fields are linear combinations of the functions
%>
%> @f[
%> u_n^m(x) = j_n(k |x|) \, Y_n^m(\hat{x}) 
%> @f]
%>
%> which are solutions to the Helmholtz equation. More precisely, we shift
%> these functions to have an arbitrary center of expansion z. Hence, an object of this
%> class represents a function
%>
%> @f[
%> u(x) = \sum_{n=0}^N \sum_{m = -n}^n \alpha_n^m \, u_n^m (x-z)
%> @f]
%>
%> Typically, the coefficients are computed by a best approximation on a
%> sphere of radius R around z. Then
%>
%> @f[
%>  \alpha_n^m = - \int_{S^2} \frac{ u(z + R \hat{x}) \, Y_n^{-m}(\hat{x}) }{ j_n(kR) } \,
%>   ds(\hat{x}) \, .
%> @f]
%
% ======================================================================
classdef EntireScalarField < fields.ScalarField
    % classdef EntireScalarField < ScalarField
    
    properties (SetAccess = protected)
        
        %> @brief The wave number
        wavenum = 1;
        
        %> @brief The degree of the highest vector spherical harmonic in
        %> the expansion.
        degree = 1;
        
        %> @brief The coordinates of the center of expansion, a 3
        %> coordinate column vector.
        z = [0; 0; 0]
        
        %> @brief The coefficients for the functions @f$ u_n^{m} @f$.
        %>
        %> These form a row vector of length (degree+1)^2 and are ordered 
        %> like
        %>
        %> a_0^0 a_1^{-1} a_1^{0} a_1^{1} a_2^{-2} ... a_2^{2} a_3^{-3} ... a_degree^{degree}
        %>
        alpha = [];
        
    end
    
    methods
        
        %> @brief Construction
        %>
        %> Precise behaviour depends on the number of arguments:
        %> - 2 arguments: field centered at origin
        %> - 3 arguments: field centered at z
        function sf = EntireScalarField(wavenum, alpha, z)
            
            if ( nargin >= 1 )
                sf.wavenum = wavenum;
            end
            if ( nargin >= 2 )
                sf.setCoeffs(alpha);                
            end
            
            if ( nargin >= 3 )
                sf.setCenter(z);
            end
            
        end
        
        %> @brief Set center of expansion 
        function setCenter(sf, z)
            
            assert(length(z) == 3, 'Center points must be vector of dimension 3.')

            sf.z = [z(1); z(2); z(3)];
            
        end
        
        %> @brief Set degree and expanson coefficients
        function setCoeffs(sf, alpha)
            
            assert(mod(sqrt(length(alpha)), 1) == 0, 'Length of coefficient vector must be a square number.');
            
            sf.alpha = alpha;
            sf.degree = sqrt(length(alpha)) - 1;            
            
        end
        
        %> @brief Implementation of ScalarField.eval()
        %>
        %> This function computes everything from scratch. No stored
        %> information is used.
        function U = eval(sf, X1, X2, X3)
            
            x1 = X1(:).' - sf.z(1);
            x2 = X2(:).' - sf.z(2);
            x3 = X3(:).' - sf.z(3);

            U = zeros(1,length(x1));

            [phi, theta, r] = cart2sph(x1, x2, x3);
            theta = pi/2 - theta;

            for n=0:sf.degree

                % Intitialize variable for spherical Bessel function
                j_n = zeros(size(r));

                % for small arguments, we have to use asymptotic forms for the
                % Bessel functions
                r_small = ( r < 1e-5 );
                j_n(r_small) = sqrt(pi / 4 ) /  gamma(n+3/2) * ( sf.wavenum * r(r_small)/2 ).^n;
                j_n(~r_small) = sqrt(pi / (2*sf.wavenum) ./ r(~r_small) ) .* besselj(n+1/2, sf.wavenum*r(~r_small));

                Y_x = specialfunc.sphericalHarmonics(theta, phi, n);

                U = U + j_n .* (sf.alpha(n^2+1:(n+1)^2) * Y_x);
            end
            
            % Reshape output to size of the input
            U = reshape(U, size(X1));

        end
        
        
        %> @brief Print norm in each degree
        %>
        %> Function for debugging purposes that prints the norm of
        %> projections onto functions of just one degree n.
        function printNormInDegree(sf)
            
            for n = 0:sf.degree
                
                mRange = n^2+1:(n+1)^2;
                fprintf('  %10.4e', sqrt(sum(abs(sf.alpha(mRange)).^2)));
            end
            
            fprintf('\n');
            
        end
        
    end
    
    methods (Static)
        
        %> @brief Compute the best approximation of a given ScalarField by
        %> an EntireScalarField.
        %>
        %> The ScalarField @a sf is approximated by an EntireScalarField
        %> within a ball of radius R centred at z. This done by computing the 
        %> best approximation  to sf in the space spanned by the basis functions 
        %> of degree less than or equal to @a N on the boundary of the ball 
        %> in the L^2-norm (i.e. compute an orthogonal projection)
        %>
        %> The function performs integration on the unit sphere with a
        %> tensor product quadrature rule, with a Gauss-Legendre rule in the 
        %> polar angle and a composite trapezoidal rule in the azimuthal angle. 
        %> 2*Nazim points in the
        %> azimuthal and Npolar points in the polar direction are used. If these
        %> arguments are not specified, a default value of 100 points in polar
        %> and 200 points in azimuthal direction is used.
        function esf = bestApprox(sf, wavenum, N, z, R, Npolar, Nazim)
                    
            % obtain the quadrature points and weights
            if ( nargin >= 6 )
                [Theta, Phi, W] = quadrature.sphereGaussLegTrap( Npolar, Nazim );
            else
                [Theta, Phi, W] = quadrature.sphereGaussLegTrap( 100, 100 );
            end         
            
            X1 = z(1) + R * cos(Phi) .* sin(Theta);
            X2 = z(2) + R * sin(Phi) .* sin(Theta);
            X3 = z(3) + R * cos(Theta);
            
            % evaluate the VectorField in the quadrature points
            F = sf.eval(X1, X2, X3);
            
            % intialize coefficients
            alpha = zeros(1,(N+1)^2);
            
            % for each vector spherical harmonic up to degree N
            for n=0:N
                
                % scaling of coefficients on sphere of radius R
                j_n = sqrt(pi / (2*wavenum) / R ) * besselj(n+1/2,wavenum*R);
                
                Wmat = W * ones(1,2*n+1);
                
                % evaluate the sh on the quadrature points
                Y = specialfunc.sphericalHarmonics(Theta, Phi, n);
                
                % scalar producs in L^2_tangential
                a = F * (Y' .* Wmat);

                % set the coefficients
                m_range = n^2+1:(n+1)^2;
                alpha(m_range) = a / j_n;                
            end
            
            % define the EntireScalarField
            esf = fields.EntireScalarField(wavenum);
            esf.setCenter(z);
            esf.setCoeffs(alpha);            
        end        
        
    end % of methods (Static)
    
end

